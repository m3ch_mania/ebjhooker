// ==UserScript==
// @name        ebjhooker
// @namespace   ebjhooker
// @version     1.3
// @icon        https://www.ebookjapan.jp/favicon.ico
// @description Grabs image data from the browser viewer to save locally. Still a bit buggy. Currently works when you click off one of the pages. Source has further comments. Last updated by m3ch_mania (07/01/2018)
// @author      m3ch_mania, Cure, eligrey, anonymous
// @updateURL   https://gitlab.com/m3ch_mania/ebjhooker/raw/master/ebjhooker.user.js
// @downloadURL https://gitlab.com/m3ch_mania/ebjhooker/raw/master/ebjhooker.user.js
// @run-at		document-start
// @include     https://br.ebookjapan.jp/br/reader/viewer/*
// @grant       GM_xmlhttpRequest
// ==/UserScript==

// Borrows code from eligrey (https://github.com/eligrey/) that's been embedded and changed due to updates to the EMCAscript standards

// Here's some usage tips
// 1. Get the reader to go in single page mode, it'll make your crop job easier later.
//	1.1. Create a transparency mask in PS, set its treshold to 255, trim transparent pixels, easy cropping done in 5 seconds or less.
// 2. If you pressed cancel at the prompt or if it didn't show at all, press the 'up' key on your keyboard to turn on autoripping
// 3. Autoripping won't stop until it gets to the end or if you make it save the same page twice.
// 4. You can just manually rip using the left arrow key to scroll
// 5. If you want to start ripping at a certain spot, just use any method to get through the book that isn't the left arrow key and it won't rip anything.
(function() {

    let f = HTMLCanvasElement.prototype.toBlob;
    let globPress;
    let pad;

    pad = function (n, width, z) {
        z = z || '0';
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    }


    function triggerKey(){
        try{
            var eventObj = document.createEvent("Events");
            eventObj.initEvent("keydown", true, true);
            eventObj.which = 37;
            eventObj.keyCode = 37;
            eventObj.key = "ArrowLeft";
            document.dispatchEvent(eventObj);
        }
        catch(e){
            console.log(e);
        }
    }

    // wait for page to load
    function domReady(){
        let pages = [];
        let pagenumber = 0; //cover has page number 0
        let pagedata = "";
        let interval;
        let pagecanvas;
        if(confirm("Dump automatically? (Press the up arrow key to turn it on later, or use the left arrow key to rip manually)")){
            interval = setInterval(triggerKey,4000);
        }
        document.addEventListener('keydown', function(e) {
            if(e.keyCode == 37){
                pagecanvas = document.querySelectorAll(".viewer > canvas:nth-of-type(1)");
                pagenumber = document.querySelector(".slider__output").innerText
                if(pages.includes(pagenumber)){
                    console.log("Saving done");
                    clearInterval(interval);
                }
                else{
                    var tempCanvas = pagecanvas[0];
                    tempCanvas.toBlob = f;
                    tempCanvas.toBlob(function(blob) {
                        let bookName = document.querySelector(".header__title > h2").innerText
                        // var bookName = $('.header__title > h2').text().replace(' ????�???�_', ' ')
                        let filename = bookName + pad(parseInt(pagenumber) + 1,4)+'.png';
                        saveAs(blob, filename);
                        //console.log("saved as "+filename);
                    });
                    pages.push(pagenumber);
                }
                //super();
            }else if(e.keyCode == 38){
                console.log("Manually turning on timer");
                interval = setInterval(triggerKey,2200);
            }
        });
    }

    // Mozilla, Opera, Webkit
    if ( document.addEventListener ) {
        document.addEventListener( "DOMContentLoaded", function(){
            document.removeEventListener( "DOMContentLoaded", arguments.callee, false);
            domReady();
        }, false );
    }

    var saveAs = saveAs || (function(view) {
	"use strict";
	// IE <10 is explicitly unsupported
	if (typeof view === "undefined" || typeof navigator !== "undefined" && /MSIE [1-9]\./.test(navigator.userAgent)) {
		return;
	}
	var
		  doc = view.document
		  // only get URL when necessary in case Blob.js hasn't overridden it yet
		, get_URL = function() {
			return view.URL || view.webkitURL || view;
		}
		, save_link = doc.createElementNS("http://www.w3.org/1999/xhtml", "a")
		, can_use_save_link = "download" in save_link
		, click = function(node) {
			var event = new MouseEvent("click");
			node.dispatchEvent(event);
		}
		, is_safari = /constructor/i.test(view.HTMLElement) || view.safari
		, is_chrome_ios =/CriOS\/[\d]+/.test(navigator.userAgent)
		, setImmediate = view.setImmediate || view.setTimeout
		, throw_outside = function(ex) {
			setImmediate(function() {
				throw ex;
			}, 0);
		}
		, force_saveable_type = "application/octet-stream"
		// the Blob API is fundamentally broken as there is no "downloadfinished" event to subscribe to
		, arbitrary_revoke_timeout = 1000 * 40 // in ms
		, revoke = function(file) {
			var revoker = function() {
				if (typeof file === "string") { // file is an object URL
					get_URL().revokeObjectURL(file);
				} else { // file is a File
					file.remove();
				}
			};
			setTimeout(revoker, arbitrary_revoke_timeout);
		}
		, dispatch = function(filesaver, event_types, event) {
			event_types = [].concat(event_types);
			var i = event_types.length;
			while (i--) {
				var listener = filesaver["on" + event_types[i]];
				if (typeof listener === "function") {
					try {
						listener.call(filesaver, event || filesaver);
					} catch (ex) {
						throw_outside(ex);
					}
				}
			}
		}
		, auto_bom = function(blob) {
			// prepend BOM for UTF-8 XML and text/* types (including HTML)
			// note: your browser will automatically convert UTF-16 U+FEFF to EF BB BF
			if (/^\s*(?:text\/\S*|application\/xml|\S*\/\S*\+xml)\s*;.*charset\s*=\s*utf-8/i.test(blob.type)) {
				return new Blob([String.fromCharCode(0xFEFF), blob], {type: blob.type});
			}
			return blob;
		}
		, FileSaver = function(blob, name, no_auto_bom) {
			if (!no_auto_bom) {
				blob = auto_bom(blob);
			}
			// First try a.download, then web filesystem, then object URLs
			var
				  filesaver = this
				, type = blob.type
				, force = type === force_saveable_type
				, object_url
				, dispatch_all = function() {
					dispatch(filesaver, "writestart progress write writeend".split(" "));
				}
				// on any filesys errors revert to saving with object URLs
				, fs_error = function() {
					if ((is_chrome_ios || (force && is_safari)) && view.FileReader) {
						// Safari doesn't allow downloading of blob urls
						var reader = new FileReader();
						reader.onloadend = function() {
							var url = is_chrome_ios ? reader.result : reader.result.replace(/^data:[^;]*;/, 'data:attachment/file;');
							var popup = view.open(url, '_blank');
							if(!popup) view.location.href = url;
							url=undefined; // release reference before dispatching
							filesaver.readyState = filesaver.DONE;
							dispatch_all();
						};
						reader.readAsDataURL(blob);
						filesaver.readyState = filesaver.INIT;
						return;
					}
					// don't create more object URLs than needed
					if (!object_url) {
						object_url = get_URL().createObjectURL(blob);
					}
					if (force) {
						view.location.href = object_url;
					} else {
						var opened = view.open(object_url, "_blank");
						if (!opened) {
							// Apple does not allow window.open, see https://developer.apple.com/library/safari/documentation/Tools/Conceptual/SafariExtensionGuide/WorkingwithWindowsandTabs/WorkingwithWindowsandTabs.html
							view.location.href = object_url;
						}
					}
					filesaver.readyState = filesaver.DONE;
					dispatch_all();
					revoke(object_url);
				}
			;
			filesaver.readyState = filesaver.INIT;

			if (can_use_save_link) {
				object_url = get_URL().createObjectURL(blob);
				setImmediate(function() {
					save_link.href = object_url;
					save_link.download = name;
					click(save_link);
					dispatch_all();
					revoke(object_url);
					filesaver.readyState = filesaver.DONE;
				}, 0);
				return;
			}

			fs_error();
		}
		, FS_proto = FileSaver.prototype
		, saveAs = function(blob, name, no_auto_bom) {
			return new FileSaver(blob, name || blob.name || "download", no_auto_bom);
		}
	;

	// IE 10+ (native saveAs)
	if (typeof navigator !== "undefined" && navigator.msSaveOrOpenBlob) {
		return function(blob, name, no_auto_bom) {
			name = name || blob.name || "download";

			if (!no_auto_bom) {
				blob = auto_bom(blob);
			}
			return navigator.msSaveOrOpenBlob(blob, name);
		};
	}

	// todo: detect chrome extensions & packaged apps
	//save_link.target = "_blank";

	FS_proto.abort = function(){};
	FS_proto.readyState = FS_proto.INIT = 0;
	FS_proto.WRITING = 1;
	FS_proto.DONE = 2;

	FS_proto.error =
	FS_proto.onwritestart =
	FS_proto.onprogress =
	FS_proto.onwrite =
	FS_proto.onabort =
	FS_proto.onerror =
	FS_proto.onwriteend =
		null;

	return saveAs;
}(
	   typeof self !== "undefined" && self
	|| typeof window !== "undefined" && window
	|| this
));
})();